﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Collections.Generic;

[Serializable]
public class TimeChanged : UnityEvent<float> { }
[Serializable]
public class CountDownChanged : UnityEvent<float> { }
[Serializable]
public class ScoreChanged : UnityEvent<int> { }
[Serializable]
public class MatchEnded : UnityEvent<int> { }

public class MatchController : MonoBehaviour
{
    private float timeToEnd;
    private float countDown;
    private int score;

    public TimeChanged timeChanged;
    public ScoreChanged scoreChanged;
    public CountDownChanged countDownChanged;
    public UnityEvent timeFrozen;
    public UnityEvent timeDefrozen;
    public UnityEvent matchStarted;
    public MatchEnded matchEnded;

    private bool isFrozen;
    private float timeToDefreeze;

    private void Awake()
    {
        isFrozen = false;
        timeToDefreeze = 0f;
        score = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        timeToEnd = GameController.instance.TotalTimeSecs;
        countDown = GameController.instance.CountDown;
    }

    // Update is called once per frame
    void Update()
    {
        if(countDown >= 0)
        {
            countDown -= Time.deltaTime;
            if(countDownChanged != null) { countDownChanged.Invoke(countDown); }
            if(countDown < 0)
            {
                if(matchStarted != null){ matchStarted.Invoke(); }   
            }
            return;
        }

        if (!isFrozen)
        {
            timeToEnd -= Time.deltaTime;
            if (timeChanged != null)
            {
                timeChanged.Invoke(timeToEnd);
            }

            if (timeToEnd <= 0f)
            {
                EndMatch();
            }
        }
        else
        {
            timeToDefreeze -= Time.deltaTime;
            if (timeToDefreeze <= 0)
            {
                isFrozen = false;
                if (timeDefrozen != null)
                {
                    timeDefrozen.Invoke();
                }
            }
        }
    }

    public void EndMatch()
    {
        int prevBestScore = PlayerPrefs.GetInt("BestScore", 0);
        if (score > prevBestScore)
        {
            PlayerPrefs.SetInt("BestScore", score);
        }

        if (matchEnded != null)
        {
            matchEnded.Invoke(score);
        }

    }

    public void Freeze()
    {
        if (!isFrozen)
        {
            isFrozen = true;
            timeToDefreeze = GameController.instance.TimeFreeze;
            if (timeFrozen != null)
            {
                timeFrozen.Invoke();
            }
        }
    }

    public void Pause()
    {
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        Time.timeScale = 1f;
    }

    public void TileDestroyed(TileSupertype superType, TileSubType subType, int row, int column)
    {
        if (superType == TileSupertype.BONUS && subType == TileSubType.TYPE2)
        {
            Freeze();
        }
        Debug.Log("Score changed");
        score += 100;

        if (scoreChanged != null)
        {
            Debug.Log("Score invoked");
            scoreChanged.Invoke(score);
        }
    }


}