﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class CellController : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerExitHandler
{
    SpriteRenderer spriteRenderer;
    Vector3 beginTouch;
    Vector3 direction;
    float magnitude;
    bool isSelected;
    BoxCollider2D boxCollider;

    public TileSwiped cellSwiped;

    private void Awake()
    {
        isSelected = false;
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();
    }

    private void Update()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void  SetContent(Sprite sprite)
    {
        spriteRenderer.sprite = sprite;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("Drag " + (x,y));
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        beginTouch = Camera.main.ScreenToWorldPoint(eventData.position);
        isSelected = true;
       // Debug.Log("PointerDown: " + (x,y));
    }

    public void OnPointerExit(PointerEventData eventData)
    {   
        if(isSelected)
        {
           // Debug.Log("PointerExit " + (x,y));
            Vector3 exitTouch = Camera.main.ScreenToWorldPoint(eventData.position);
            direction = exitTouch - beginTouch;
            magnitude = direction.magnitude;
            direction = direction.normalized;

            DragDirection dragDirection = DragDirection.None;

            float strafe = Vector3.Project(direction, Vector3.left).x;
            float forward = Vector3.Project(direction, Vector3.up).y;

            if (strafe > 0.9)
            {
                dragDirection = DragDirection.Right;
            }
            else if (strafe < -0.9)
            {
                dragDirection = DragDirection.Left;
            }
            else if (forward > 0.9)
            {
                dragDirection = DragDirection.Up;
            }
            else if (forward < -0.9)
            {
                dragDirection = DragDirection.Down;
            }

            if (cellSwiped != null && dragDirection != DragDirection.None)
            {
                cellSwiped.Invoke(dragDirection, x, y);
            }

            isSelected = false;
        }
        
    }


}*/
