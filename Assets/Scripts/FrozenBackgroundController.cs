﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrozenBackgroundController : MonoBehaviour
{
    [SerializeField]
    Animator frozenBackgroundAnimator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Freeze()
    {
        frozenBackgroundAnimator.SetBool("isFrozen", true);
    }

    public void Defreeze()
    {
        frozenBackgroundAnimator.SetBool("isFrozen", false);
    }
}
