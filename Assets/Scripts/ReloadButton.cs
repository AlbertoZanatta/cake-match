﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadButton : MonoBehaviour
{
    public void Reload()
    {
        GameController.instance.StartGame();
    }

}
