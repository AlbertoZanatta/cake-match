﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class TileDestroyed : UnityEvent<TileSupertype, TileSubType, int, int> { }
public class Grid : MonoBehaviour
{
    private int numRows;
    private int numColumns;
    private int minMatchLength;
    private float bonusOdds;
    private Dictionary<TileSupertype, Dictionary<TileSubType, Sprite>> tileTypesDict;
    [SerializeField]
    private TileSubType bonusTileType;

    [SerializeField]
    private GameObject cellPrefab;
    [SerializeField]
    private GameObject tilePrefab;

    private Cell[,] grid;

    public TileDestroyed destroyedTile;
    public UnityEvent noMoreMatches; 
    private bool canMove;
    // Start is called before the first frame update
    private void Awake()
    {
        canMove = false;
    }

    void Start()
    {
        Debug.Log("Grid Awake");
        InitFromController();
        InitGrid();
    }

    public void StartGame()
    {
        FillBoard();
    }


    private void FillBoard()
    {
        bool instantiateFirstRow = InstantiateFirstRow();
        bool compact = Compact();
        if(instantiateFirstRow || compact)
        {
            Invoke("FillBoard", 1f / GameController.instance.TileSpeed);
        }
        else
        {
            Invoke("ManageMatches", 1f / GameController.instance.TileSpeed);
        }
    }

    private bool InstantiateFirstRow()
    {
        bool instantiatedTiles = false;
        for (int column = 0; column < numColumns; column++)
        {
            if(grid[0,column].IsEmpty())
            {
                instantiatedTiles = true;
                InstantiateTile(0, column);
            }     
        }
        return instantiatedTiles;
    }

    private bool Compact()
    {
        bool movedTiles = false;
        for (int row = numRows - 2; row >= 0; row--)
        {
            for (int column = numColumns - 1; column >= 0; column--)
            {
                Cell cell = grid[row, column];
                Vector2Int underCellCoords = GetAdjCellCoordinates(DragDirection.Down, row, column);
                if (underCellCoords.x > 0 && underCellCoords.y >= 0)
                {
                    Cell underCell = grid[underCellCoords.x, underCellCoords.y];
                    if (!cell.IsEmpty() && underCell.IsEmpty())
                    {
                        Tile tile = cell.Tile;
                        cell.RemoveTile();
                        underCell.SetTile(tile);
                        tile.MoveTile(GetWorldPosition(underCellCoords.x, underCellCoords.y));
                        movedTiles = true;
                    }

                }
            }
        }
        return movedTiles;
    }

    private void InitFromController()
    {
        numRows = GameController.instance.NumRows;
        numColumns = GameController.instance.NumColumns;
        minMatchLength = GameController.instance.MinMatchLength;
        bonusTileType = GameController.instance.BonusTileType;
        bonusOdds = GameController.instance.BonusOdds;
    }

    private void InitGrid()
    {
        grid = new Cell[numRows, numColumns];

        for (int row = 0; row < numRows; row++)
        {
            for (int column = 0; column < numColumns; column++)
            {
                Vector2 worldPosition = GetWorldPosition(row, column);
                GameObject cellObject = Instantiate(cellPrefab, new Vector3(worldPosition.x, worldPosition.y, 0), Quaternion.identity);
                cellObject.transform.parent = this.gameObject.transform;
                Cell cell = cellObject.GetComponent<Cell>();
                cell.Init(row, column);
                cell.cellSwiped.AddListener(CellSwiped);
                grid[row, column] = cell;
            }
        }

    }

    public void CellSwiped(DragDirection direction, int row, int column)
    {
        if(canMove)
        {
            Vector2Int adjCellCoords = GetAdjCellCoordinates(direction, row, column);
            if (adjCellCoords.x < 0 || adjCellCoords.y < 0)
                return;
            else
            {
                canMove = false;
                SwitchTiles(new Vector2Int(row, column), adjCellCoords);

                bool matchesFound = ManageMatches();
                if (!matchesFound)
                {
                    StartCoroutine(SwitchBack(new Vector2Int(row, column), adjCellCoords));
                }
            }
        }   
    }

    private bool ManageMatches()
    {
        List<Match> matches = FindAllMatches();
        if(matches.Count == 0)
        {
            if(!CheckAnyCombination())
            {
                if (noMoreMatches != null) { noMoreMatches.Invoke(); }
            }
            else
            {
                canMove = true;
            }
            return false;

        }
        else
        {
            foreach (Match match in matches)
            {
                foreach (Vector2Int cellIndex in match.Indices)
                {
                    ManageCell(cellIndex);
                }
            }

            Invoke("FillBoard", 1f / GameController.instance.TileSpeed);
            return true;
        }
    }

    private void ManageCell(Vector2Int cellIndex)
    {
        if(cellIndex.x < 0 || cellIndex.y < 0)
        {
            return;
        }

        Cell cell = grid[cellIndex.x, cellIndex.y];
        Tile tile = cell.Tile;
        if(!tile || !cell)
        {
            return;
        }
        else
        {
            TileSupertype superType = tile.TileType.TileSuperType;
            TileSubType subType = tile.TileType.TileSubType;
            if (destroyedTile != null)
            {
                destroyedTile.Invoke(superType, subType, cellIndex.x, cellIndex.y);
            }

            cell.DestroyTile();

            if (superType == TileSupertype.BONUS && subType == TileSubType.TYPE1)
            {
                ManageCell(GetAdjCellCoordinates(DragDirection.Down, cellIndex.x, cellIndex.y));
                ManageCell(GetAdjCellCoordinates(DragDirection.Up, cellIndex.x, cellIndex.y));
                ManageCell(GetAdjCellCoordinates(DragDirection.Left, cellIndex.x, cellIndex.y));
                ManageCell(GetAdjCellCoordinates(DragDirection.Right, cellIndex.x, cellIndex.y));
            }
           
        }

    }

    private IEnumerator SwitchBack(Vector2Int cellIndex1, Vector2Int cellIndex2)
    {
        yield return new WaitForSeconds(1f / GameController.instance.TileSpeed);
        SwitchTiles(cellIndex1, cellIndex2);
        canMove = true;

    }

    private void SwitchTiles(Vector2Int cellIndex1, Vector2Int cellIndex2)
    {
        int row1 = cellIndex1.x;
        int column1 = cellIndex1.y;

        int row2 = cellIndex2.x;
        int column2 = cellIndex2.y;

        Cell cell1 = grid[row1, column1];
        Cell cell2 = grid[row2, column2];

        Tile tile1 = cell1.Tile;
        Tile tile2 = cell2.Tile;

        cell1.RemoveTile();
        cell2.RemoveTile();

        cell1.SetTile(tile2);
        cell2.SetTile(tile1);

        tile1.MoveTile(GetWorldPosition(row2, column2));
        tile2.MoveTile(GetWorldPosition(row1, column1));
    }

    private Vector2Int GetAdjCellCoordinates(DragDirection direction, int row, int column)
    {
        Vector2Int adjCellCoords = new Vector2Int();
        switch(direction)
        {
            case DragDirection.Up:
                adjCellCoords = new Vector2Int(row - 1, column);
                break;
            case DragDirection.Down:
                adjCellCoords = new Vector2Int(row + 1, column);
                break;
            case DragDirection.Left:
                adjCellCoords = new Vector2Int(row, column - 1);
                break;
            case DragDirection.Right:
                adjCellCoords = new Vector2Int(row, column + 1);
                break;
            case DragDirection.None:
                adjCellCoords = new Vector2Int(-1, -1);
                break;
        }

        if(adjCellCoords.x < 0 || adjCellCoords.y < 0 || adjCellCoords.x >= numRows || adjCellCoords.y >= numColumns)
            adjCellCoords = new Vector2Int(-1, -1);
        return adjCellCoords;
    }

    public void InstantiateTile(int row, int column)
    {
        GameObject tileObject = GameObject.Instantiate(tilePrefab, GetWorldPosition(row, column), Quaternion.identity);
        float odds = UnityEngine.Random.Range(0f, 1f);
        TileType tileType = odds <= bonusOdds ? new TileType(TileSupertype.BONUS, bonusTileType): new TileType(TileSupertype.NORMAL, GameController.instance.GetRandomSubType(TileSupertype.NORMAL));
        Tile tile = tileObject.GetComponent<Tile>();
        tile.Init(tileType);
        grid[row, column].SetTile(tile);
    }

    public Vector3 GetWorldPosition(int row, int column)
    {
        float x = transform.position.x - numRows / 2.0f + column + 0.5f;
        float y = transform.position.y + numColumns / 2.0f - row;
        return new Vector3(x, y, 0f);
    }

    public Vector2Int GetGridPosition(int x, int y)
    {
        int row = (int)(-y + transform.position.y + numColumns / 2.0f);
        int column = (int)(x - transform.position.x + numRows / 2.0f - 0.5f);
        return new Vector2Int(row, column);
    }

    private List<Match> FindAllMatches()
    {
        List<Match> matches = new List<Match>();
        matches.AddRange(FindAllRowMatches());
        matches.AddRange(FindAllColumnMatches());
        //Eventualmente inserire altri tipi di match
        return matches;
    }

    private List<Match> FindAllColumnMatches()
    {
        List<Match> matches = new List<Match>();
        for (int column = 0; column < numColumns; column++)
        {
            matches.AddRange(FindColumnMatches(column));
        }
        return matches;
    }

    private List<Match> FindColumnMatches(int column)
    {
        List<Vector2Int> columnIndices = new List<Vector2Int>();
        for(int row = 0; row < numRows; row++)
        {
            columnIndices.Add(new Vector2Int(row, column));
        }
        return FindMatches(columnIndices);
    }

    private List<Match> FindAllRowMatches()
    {
        List<Match> matches = new List<Match>();
        for (int row = 0; row < numRows; row++)
        {
            matches.AddRange(FindRowMatches(row));
        }
        return matches;
    }

    private List<Match> FindRowMatches(int row)
    {
        List<Vector2Int> rowIndices = new List<Vector2Int>();
        for (int column = 0; column < numColumns; column++)
        {
            rowIndices.Add(new Vector2Int(row, column));
        }
        return FindMatches(rowIndices);
    }

    bool IsEqualType(TileType type1, TileType type2)
    {
        if (type1.TileSuperType == TileSupertype.BONUS || type2.TileSuperType == TileSupertype.BONUS)
            return true;
        else
            return type1.TileSubType == type2.TileSubType;
    }

    private List<Match> FindMatches(List<Vector2Int> indices)
    {
        List<Match> matches = new List<Match>();
        for(int i = 0; i < indices.Count;)
        {
            List<Vector2Int> matchIndices = new List<Vector2Int>();
            matchIndices.Add(indices[i]);
            int j = i;
            TileType matchType = grid[indices[i].x, indices[i].y].Tile.TileType;
            for (; j < indices.Count-1 && IsEqualType(matchType, grid[indices[j+1].x, indices[j+1].y].Tile.TileType); j++)
            {
                matchIndices.Add(indices[j + 1]);
                if(matchType.TileSuperType == TileSupertype.BONUS)
                {
                    matchType = grid[indices[j + 1].x, indices[j + 1].y].Tile.TileType;
                }
            }
            if(matchIndices.Count >= minMatchLength)
            {
                matches.Add(new Match(matchIndices));
                i = j + 1;
            }
            else
            {
                i = i + 1;
            }
        }
        return matches;
    }

    private bool CheckAnyCombination()
    {
        bool matchFound = false;
        for(int row = 0; row < numRows; row ++)
        {
            for(int col = 0; col < numColumns; col++)
            {
                foreach(DragDirection direction in Enum.GetValues(typeof(DragDirection)))
                {
                    Vector2Int adjCell = GetAdjCellCoordinates(direction, row, col);
                    if (adjCell.x > 0 && adjCell.y > 0)
                    {
                        Cell cell1 = grid[row, col];
                        Cell cell2 = grid[adjCell.x, adjCell.y];

                        Tile tile1 = cell1.Tile;
                        Tile tile2 = cell2.Tile;

                        cell1.RemoveTile();
                        cell2.RemoveTile();

                        cell1.SetTile(tile2);
                        cell2.SetTile(tile1);

                        List<Match> rowMatches = FindRowMatches(adjCell.x);
                        List<Match> columnMatches = FindColumnMatches(adjCell.y);

                        cell1.RemoveTile();
                        cell2.RemoveTile();

                        cell1.SetTile(tile1);
                        cell2.SetTile(tile2);

                        if(rowMatches.Count > 0 || columnMatches.Count > 0)
                        {
                            matchFound = true;
                            Debug.Log("" + row + ", " + col);
                            break;
                        }
                    }

                }


            }
        }

        return matchFound;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public class Match
{
    List<Vector2Int> indices;
    public List<Vector2Int> Indices { get => indices;}

    public Match(List<Vector2Int> indices)
    {
        this.indices = indices;
    }

    
}
