﻿/*using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class CellChangedEvent : UnityEvent<CellType, int, int> {}
[Serializable]
public class SwitchCells : UnityEvent<Vector2Int, Vector2Int> {}
[Serializable]
public class MatchFoundEvent : UnityEvent<Match> { }
public class CellType
{
    private TileSupertype _cellSuperType;
    public TileSupertype cellSuperType
    {
        get { return _cellSuperType; }
    }
    private TileSubType _cellSubType;
    public TileSubType cellSubType
    {
        get { return _cellSubType; }
    }

    public CellType(TileSupertype cellSuperType, TileSubType cellSubType = TileSubType.TYPE1)
    {
        this._cellSuperType = cellSuperType;
        this._cellSubType = cellSubType;
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public bool Equals(CellType other)
    {
        return other.cellSuperType == _cellSuperType && other.cellSubType == _cellSubType;
    }
}

public class BoardController : MonoBehaviour
{
    [SerializeField]
    GameSettings gameSettings;

    int numRows;
    int numColumns;
    int minMatchLength;
    int totalCells;
    private CellType[,] cellList;
    
    //Events
    public CellChangedEvent cellChanged;
    public MatchFoundEvent matchFound;
    public SwitchCells switchCells;

    private void Awake()
    {
        
    }

    private void Start()
    {
        numRows = gameSettings.NumRows;
        numColumns = gameSettings.NumColumns;
        minMatchLength = gameSettings.MinMatchLength;
        totalCells = numRows * numColumns;
        Debug.Log("Total cells: " + totalCells);
        List<TileSubType> normalCellTypes = (from normalSubtype in gameSettings.NormalTypeSprites select normalSubtype.type).ToList();
        cellList = new CellType[numRows, numColumns];

        //Dummy initialization
        for (int x = 0; x < numRows; x++)
        {
            for (int y = 0; y < numColumns; y++)
            {
                int randomIndex = UnityEngine.Random.Range(0, normalCellTypes.Count);
                CellType cellType = new CellType(TileSupertype.EMPTY);
                cellList[x, y] = cellType;
                if (cellChanged != null)
                {
                    cellChanged.Invoke(cellType, x, y);
                }
            }
        }

        InitializeBoard(normalCellTypes);
    }

    private void InitializeBoard(List<TileSubType> normalCellTypes)
    {
        for (int x = 0; x < numRows; x++)
        {
            for(int y = 0; y < numColumns; y++)
            {
                int randomIndex = UnityEngine.Random.Range(0, normalCellTypes.Count);
                CellType cellType = new CellType(TileSupertype.NORMAL, normalCellTypes[randomIndex]);
                cellList[x, y] = cellType;
                if(cellChanged != null)
                {
                    cellChanged.Invoke(cellType, x, y);
                }
            }
        }
    }

    void SwitchCells(int index1, int index2)
    {
        int x1 = index1 / numColumns;
        int y1 = index1 % numColumns;
        int x2 = index2 / numColumns;
        int y2 = index2 % numColumns;
        CellType cell1 = cellList[x1, y1];
        CellType cell2 = cellList[x2, y2];
        cellList[x1, y1] = cell2;
        cellList[x2, y2] = cell1;
        if (switchCells != null)
        {
            switchCells.Invoke(new Vector2Int(x1, y1), new Vector2Int(x2, y2));
        }
    }

    public void CheckCellExchange(DragDirection direction, int x, int y)
    {
        Debug.Log("NumColumns, NumRows: " + (numRows, numColumns));
        int cellIndex = (x * numColumns) + y;
        int adjCellIndex = GetAdiacentCellIndex(direction, cellIndex);
        Debug.Log("Cell index: " + cellIndex +" " + (x,y) + ", adjIndex: " + adjCellIndex + ", direction: " + direction);
        if (adjCellIndex >= 0)
        {
            SwitchCells(cellIndex, adjCellIndex);
            CheckForMatches(adjCellIndex/numColumns, adjCellIndex%numColumns);
        }
    }

    int GetAdiacentCellIndex(DragDirection direction, int cellIndex)
    {
        int adjIndex = -1;
        if (cellIndex >= 0 && cellIndex < totalCells && direction != DragDirection.None)
        {
            switch (direction)
            {
                case DragDirection.Up:
                    adjIndex = cellIndex - numColumns;
                    if (adjIndex < 0)
                    {
                        adjIndex = -1;
                    }
                    break;
                case DragDirection.Down:
                    adjIndex = cellIndex + numColumns;
                    if (adjIndex >= totalCells)
                    {
                        adjIndex = -1;
                    }
                    break;
                case DragDirection.Right:
                    adjIndex = cellIndex + 1;
                    if (adjIndex % numColumns == 0)
                    {
                        adjIndex = -1;
                    }
                    break;
                case DragDirection.Left:
                    if (cellIndex % numColumns == 0)
                    {
                        adjIndex = -1;
                    }
                    else
                    {
                        adjIndex = cellIndex - 1;
                    }
                    break;
            }
        }
        return adjIndex;
    }

    private List<Match> FindRowMatches(int row)
    {
        List<Vector2Int> indices = new List<Vector2Int>();
        for (int column = 0; column < numColumns; column++)
        {
            indices.Add(new Vector2Int(row, column));
        }

        List<Match> matches = FindMatches(indices);
        return matches;
    }

    private List<Match> FindColumnMatches(int column)
    {
        List<Vector2Int> indices = new List<Vector2Int>();
        for (int row = 0; row < numRows; row++)
        {
            indices.Add(new Vector2Int(row, column));
        }

        List<Match> matches = FindMatches(indices);
        return matches;
    }

    //Precondizione: Tutte le caselle hanno un tipo-cella assegnato che non è None
    List<Match> FindMatches(List<Vector2Int> cellIndices)
    {
        List<Match> matches = new List<Match>();
        int matchLength = 0;
        int beginIndex = -1;
        CellType matchType = new CellType(TileSupertype.EMPTY);
        for (int i = 0; i < cellIndices.Count; i++)
        {
            CellType currentCellType = cellList[cellIndices[i].x, cellIndices[i].y];
            if (currentCellType.Equals(matchType) || currentCellType.cellSuperType == TileSupertype.BONUS)
            {
                matchLength += 1;

                if (i + 1 == cellIndices.Count && matchLength >= minMatchLength)
                {
                    List<Vector2Int> matchIndices = new List<Vector2Int>();
                    List<CellType> matchTypes = new List<CellType>();
                    for (int j = beginIndex; j < beginIndex + matchLength; j++)
                    {
                        matchIndices.Add(cellIndices[j]);
                        matchTypes.Add(cellList[cellIndices[j].x, cellIndices[j].y]);
                    }
                    Match match = new Match(matchTypes, matchIndices);
                    matches.Add(match);
                }
            }
            else
            {
                if(matchLength >= minMatchLength)
                {
                    List<Vector2Int> matchIndices = new List<Vector2Int>();
                    List<CellType> matchTypes = new List<CellType>();
                    for (int j = beginIndex; j < beginIndex + matchLength; j++)
                    {
                        matchIndices.Add(cellIndices[j]);
                        matchTypes.Add(cellList[cellIndices[j].x, cellIndices[j].y]);
                    }
                    Match match = new Match(matchTypes, matchIndices);
                    matches.Add(match);
                }
                beginIndex = i;
                matchLength = 1;
                matchType = cellList[cellIndices[i].x, cellIndices[i].y];
            }
        }

        return matches;
    }

    //Checks for matches across the whole board
    private List<Match> FindMatches()
    {
        List<Match> matches = new List<Match>();
        for (int i = 0; i < numColumns; i++)
        {
            List<Match> columnMatches = FindColumnMatches(i);
            matches.AddRange(columnMatches);
        }

        for (int i = 0; i < numRows; i++)
        {
            List<Match> rowMatches = FindRowMatches(i);
            matches.AddRange(rowMatches);
        }
        return matches;
    }

    //Check for matches only along the row/column where the cell is positioned
    private void CheckForMatches(int row, int column)
    { 
        Debug.Log("Row: " + row + ", column: " + column);
        List<Match> matches = FindRowMatches(row);
        matches.AddRange(FindColumnMatches(column));
        ManageMatches(matches);

    }

    private void ManageMatches(List<Match> matches)
    {
        while (matches.Count > 0)
        {
            foreach (Match match in matches)
            {
                List<Vector2Int> matchIndices = match.cellIndices;
                foreach (Vector2Int index in matchIndices)
                {
                    CellType cellType = new CellType(TileSupertype.EMPTY);
                    cellList[index.x, index.y] = cellType;
                    if (cellChanged != null)
                    {
                        cellChanged.Invoke(cellType, index.x, index.y);
                    }
                }
            }
            RefillBoard(matches);
            matches = new List<Match>();
            //matches = FindMatches();
        }

    }

    private void RefillBoard(List<Match> matches)
    {
        List<Vector2Int> matchIndices = new List<Vector2Int>();
        foreach (Match match in matches)
        {
            matchIndices = matchIndices.Union(match.cellIndices).ToList();
        }
        List<int> tempList = (from index in matchIndices select index.x * numColumns + index.y).ToList();
        tempList.Sort();
        tempList.Reverse();
        matchIndices = (from index in tempList select new Vector2Int(index/numColumns, index%numRows)).ToList();

        foreach (Vector2Int index in matchIndices)
        {
            FillHole(index.x, index.y);
        }

    }

    private void FillHole(int row, int column)
    {
        Debug.Log("Filling hole at" + (row, column));
        if (cellList[row, column].cellSuperType == TileSupertype.EMPTY)
        {
            int adjCellIndex = GetAdiacentCellIndex(DragDirection.Up, row * numColumns + column);
            if (adjCellIndex >= 0)
            {
                int adjRow = adjCellIndex / numColumns;
                int adjColumn = adjCellIndex % numColumns;
                if (cellList[adjRow, adjColumn].cellSuperType == TileSupertype.EMPTY)
                {
                    FillHole(adjRow, adjColumn);
                }
                Debug.Log("Switched cell " + (row, column) + "with type" + cellList[row, column].cellSuperType + "and cell " + (adjRow, adjColumn) + "with type " + cellList[adjRow, adjColumn].cellSuperType);
                SwitchCells(row * numColumns + column, adjCellIndex);
                FillHole(adjRow, adjColumn);
            }
            else
            {
                List<TileSubType> normalCellTypes = (from normalSubtype in gameSettings.NormalTypeSprites select normalSubtype.type).ToList();
                int randomIndex = UnityEngine.Random.Range(0, normalCellTypes.Count);
                CellType cellType = new CellType(TileSupertype.NORMAL, normalCellTypes[randomIndex]);
                cellList[row, column] = cellType;
                if (cellChanged != null)
                {
                    cellChanged.Invoke(cellType, row, column);
                }
                Debug.Log("New item at cell " + (row, column));
            }


        }

    }

    /*
   

    

    

    private void MoveTillEnd(int i, DragDirection direction)
    {
        int adjCellIndex = GetAdiacentCellIndex(direction, i);
        if(adjCellIndex >= 0)
        {
            SwitchCells(i, adjCellIndex);
            MoveTillEnd(adjCellIndex, direction);
        }
    }


}


public class Match
{
    public List<CellType> cellTypes;
    public List<Vector2Int> cellIndices;

    public Match(List<CellType> cellTypes, List<Vector2Int> cellIndices)
    {
        this.cellTypes = cellTypes;
        this.cellIndices = cellIndices;
    }
}*/
