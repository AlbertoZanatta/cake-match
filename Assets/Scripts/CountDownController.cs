﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountDownController : MonoBehaviour
{
    private Animator animator;
    private TextMeshProUGUI text;
    // Start is called before the first frame update

    private void Awake()
    {
        animator = GetComponent<Animator>();
        text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CountDown(float countDown)
    {
        if(countDown < 0)
        {
            animator.SetTrigger("Hide");
        }
        else
        {
            int countDownInt = Mathf.CeilToInt(countDown);
            text.SetText(countDownInt.ToString());
        }



    }
}
