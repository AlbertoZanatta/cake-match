﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;
using TMPro;

public class CellIsEmpty : UnityEvent<int, int> { }
public class CellHasTile : UnityEvent<int, int> { }
public class CellSwiped : UnityEvent<DragDirection, int, int> { }

public class Cell : MonoBehaviour
{ 
    private int column;
    private int row;
    private Tile tile;

    public int Column { get => column; }
    public int Row { get => row; }
    public Tile Tile { get => tile;}

    public CellSwiped cellSwiped;
    public CellIsEmpty cellIsEmpty;

    [SerializeField]
    private Animator explosionAnimator;
    [SerializeField]
    private Animator scoreAnimator;
    [SerializeField]
    TextMeshPro scoreText;

    private void Awake()
    {
        cellSwiped = new CellSwiped();
        cellIsEmpty = new CellIsEmpty();
    }

    public void Init(int row, int column)
    {
        this.row = row;
        this.column = column;
    }

    public void SetTile(Tile tile)
    {
        this.tile = tile;
        tile.tileSwiped.AddListener(TileSwiped);
    }

    public void RemoveTile()
    {
        if(this.tile != null)
        {
            tile.tileSwiped.RemoveListener(TileSwiped);
            tile = null;
        }
    }

    public bool IsEmpty()
    {
        return Tile == null;
    }

    public void TileSwiped(DragDirection direction)
    {
        if(cellSwiped != null)
        {
            cellSwiped.Invoke(direction, row, column);
        }
    }


    private void ShowExplosion(Tile tileToDestroy)
    {
        if (tileToDestroy.TileType.TileSuperType == TileSupertype.NORMAL)
        {
            explosionAnimator.SetTrigger("NormalExplosion");
        }
        else if (tileToDestroy.TileType.TileSuperType == TileSupertype.BONUS)
        {
            if (tileToDestroy.TileType.TileSubType == TileSubType.TYPE1)
            {
                explosionAnimator.SetTrigger("BombExplosion");
            }
            else if (tileToDestroy.TileType.TileSubType == TileSubType.TYPE2)
            {
                explosionAnimator.SetTrigger("IceExplosion");
            }
        }

    }

    private void ShowScore(TileType type)
    {
        scoreText.text = "100";
        scoreAnimator.SetTrigger("Hoist");
    }

    public void DestroyTile()
    {
        Tile tileToDestroy = this.tile;
        RemoveTile();
        if(tileToDestroy != null)
        {
            ShowExplosion(tileToDestroy);
            ShowScore(tileToDestroy.TileType);
            Destroy(tileToDestroy.gameObject);
        } 
    }
}
