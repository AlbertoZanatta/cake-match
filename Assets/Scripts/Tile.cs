﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public enum DragDirection
{
    Up,
    Down,
    Left,
    Right,
    None
}

[System.Serializable]
public class TileSwiped : UnityEvent<DragDirection> { }

public class TileType 
{
    private TileSupertype tileSuperType;
    public TileSupertype TileSuperType
    {
        get { return tileSuperType; }
    }
    private TileSubType tileSubType;
    public TileSubType TileSubType
    {
        get { return tileSubType; }
    }

    public TileType(TileSupertype cellSuperType, TileSubType cellSubType)
    {
        this.tileSuperType = cellSuperType;
        this.tileSubType = cellSubType;
    }

    public bool Equals(TileType other)
    {
        return this.tileSuperType == other.TileSuperType && this.tileSubType == other.TileSubType;
    }
}

public class Tile : MonoBehaviour, IPointerDownHandler, IPointerExitHandler
{
    TileType tileType;
    SpriteRenderer spriteRenderer;
    
    Vector3 beginTouch;
    Vector3 direction;
    float magnitude;
    bool isSelected;
    bool isMoving;
    Vector3 targetPosition;
    BoxCollider2D boxCollider;

    public TileSwiped tileSwiped;

    public TileType TileType { get => tileType;}

    private void Awake()
    {
        isSelected = false;
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();
        tileSwiped = new TileSwiped();
    }

    private void Update()
    {
        if(isMoving)
        {
            Move();
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    private void Move()
    {
        float step = GameController.instance.TileSpeed * Time.deltaTime; // calculate distance to move
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);

        // Check if the position of the cube and sphere are approximately equal.
        if (Vector3.Distance(transform.position, targetPosition) < 0.001f)
        {
            this.transform.position = targetPosition;
            isMoving = false;
            boxCollider.enabled = true;
        }
    }

    public void Init(TileType tileType)
    {
        this.tileType = tileType;
        spriteRenderer.sprite = GameController.instance.TileTypesDict[tileType.TileSuperType][tileType.TileSubType];
    }

    public void MoveTile(Vector3 newPosition)
    {
        //Debug.Log("Move tile to: " + newPosition);
        this.targetPosition = newPosition;
        isMoving = true;
        boxCollider.enabled = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        beginTouch = Camera.main.ScreenToWorldPoint(eventData.position);
        isSelected = true;
        Debug.Log("PointerDown");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (isSelected)
        {
            Debug.Log("PointerExit ");
            Vector3 exitTouch = Camera.main.ScreenToWorldPoint(eventData.position);
            direction = exitTouch - beginTouch;
            magnitude = direction.magnitude;
            direction = direction.normalized;

            DragDirection dragDirection = DragDirection.None;

            float strafe = Vector3.Project(direction, Vector3.left).x;
            float forward = Vector3.Project(direction, Vector3.up).y;

            if (strafe > 0.9)
            {
                dragDirection = DragDirection.Right;
            }
            else if (strafe < -0.9)
            {
                dragDirection = DragDirection.Left;
            }
            else if (forward > 0.9)
            {
                dragDirection = DragDirection.Up;
            }
            else if (forward < -0.9)
            {
                dragDirection = DragDirection.Down;
            }
            isSelected = false;

            if(tileSwiped != null)
            {
                tileSwiped.Invoke(dragDirection);
            }
        }

    }

}
