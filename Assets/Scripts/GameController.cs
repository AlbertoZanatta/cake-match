﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    public static GameController instance;
    [SerializeField]
    private GameSettings gameSettings;
    [SerializeField]
    Animator transitionCanvas;

    private int numRows;
    private int numColumns;
    private int minMatchLength;
    private float timeFreeze;
    private float tileSpeed;
    private float bonusOdds;
    private float totalTimeSecs;
    private float countDown;
    private TileSubType bonusTileType;
    private Dictionary<TileSupertype, Dictionary<TileSubType, Sprite>> tileTypesDict;

    public Dictionary<TileSupertype, Dictionary<TileSubType, Sprite>> TileTypesDict { get => tileTypesDict; }
    public float BonusOdds { get => bonusOdds;}
    public int MinMatchLength { get => minMatchLength;}
    public int NumColumns { get => numColumns;}
    public int NumRows { get => numRows;}
    public float TileSpeed { get => tileSpeed;}
    public float TimeFreeze { get => timeFreeze; }
    public float TotalTimeSecs { get => totalTimeSecs;}
    public TileSubType BonusTileType { get => bonusTileType;}
    public float CountDown { get => countDown;}


    void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);

        InitFromGameSettings();
    }

    private void InitFromGameSettings()
    {
        numRows = gameSettings.NumRows;
        numColumns = gameSettings.NumColumns;
        minMatchLength = gameSettings.MinMatchLength;
        bonusOdds = gameSettings.BonusOdds;
        totalTimeSecs = gameSettings.TotalTimeSecs;
        timeFreeze = gameSettings.TotalTimeFrozen;
        tileSpeed = gameSettings.TileSpeed;
        countDown = gameSettings.CountDownTime;
  
        //By default a 4x4 grid
        if (numRows <= 0){numRows = 4;}
        if (numColumns <= 0){numColumns = 4;}
        if (minMatchLength <= 0){minMatchLength = 3;}
        if (BonusOdds < 0f){bonusOdds = 0f;}
        else if (BonusOdds > 1f){bonusOdds = 1f;}

        InitTileTypesFromSettings();
    }

    private void InitTileTypesFromSettings()
    {
        Dictionary<TileSubType, Sprite> normalTypesDict = new Dictionary<TileSubType, Sprite>();
        Dictionary<TileSubType, Sprite> bonusTypesDict = new Dictionary<TileSubType, Sprite>();

        for (int i = 0; i < gameSettings.NormalTypeSprites.Count; i++)
        {
            GameSettings.CellTypeSprite cellTypeSprite = gameSettings.NormalTypeSprites[i];
            normalTypesDict.Add(cellTypeSprite.type, cellTypeSprite.sprite);
        }

        for (int i = 0; i < gameSettings.BonusTypeSprites.Count; i++)
        {
            GameSettings.CellTypeSprite cellTypeSprite = gameSettings.BonusTypeSprites[i];
            bonusTypesDict.Add(cellTypeSprite.type, cellTypeSprite.sprite);
        }

        tileTypesDict = new Dictionary<TileSupertype, Dictionary<TileSubType, Sprite>>();
        tileTypesDict.Add(TileSupertype.NORMAL, normalTypesDict);
        tileTypesDict.Add(TileSupertype.BONUS, bonusTypesDict);

    }

    public TileSubType GetRandomSubType(TileSupertype supertype)
    {
        List<TileSubType> subTypesList = tileTypesDict[supertype].Keys.ToList();
        int randomIndex = UnityEngine.Random.Range(0, subTypesList.Count);
        return subTypesList[randomIndex];
    }


    public void StartGame()
    {
        transitionCanvas.SetTrigger("Show");
        AsyncOperation operation = SceneManager.LoadSceneAsync("GameScene");
        operation.completed += LoadingComplete;

    }

    public void LoadingComplete(AsyncOperation operation)
    {
        transitionCanvas.SetTrigger("Hide");
    }

    public void EndGame(int score)
    {
        transitionCanvas.SetTrigger("Show");
        AsyncOperation operation = SceneManager.LoadSceneAsync("MenuScene");
        operation.completed += LoadingComplete;
    }

    public void SetBonusType(TileSubType subType)
    {
        this.bonusTileType = subType;
    }
}
