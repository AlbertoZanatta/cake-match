﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileSupertype
{
    NORMAL,
    BONUS
}

public enum TileSubType
{
    TYPE1,
    TYPE2,
    TYPE3,
    TYPE4,
    TYPE5,
    TYPE6
}

[CreateAssetMenu(menuName = "ScriptableObjects/GameSettingsScriptableObject")]
public class GameSettings : ScriptableObject
{
    [System.Serializable]
    public class CellTypeSprite
    {
        public TileSubType type;
        public Sprite sprite;
    }

    [SerializeField]
    private int numRows;
    public int NumRows
    {
        get { return numRows; }
    }

    [SerializeField]
    private int numColumns;
    public int NumColumns
    {
        get { return numColumns; }
    }
    [SerializeField]
    private int minMatchLength;
    public int MinMatchLength
    {
        get { return minMatchLength; }
    }
    [SerializeField]
    private float totalTimeSecs;
    public float TotalTimeSecs
    {
        get { return totalTimeSecs; }
    }

    [SerializeField]
    private float totalTimeFrozen;
    public float TotalTimeFrozen
    {
        get { return totalTimeFrozen; }
    }

    [SerializeField]
    private float tileSpeed;
    public float TileSpeed { get => tileSpeed; }

    [SerializeField]
    private float bonusOdds;
    public float BonusOdds { get => bonusOdds;}

    [SerializeField]
    private List<CellTypeSprite> normalTypeSprites;
    public List<CellTypeSprite> NormalTypeSprites
    {
        get { return normalTypeSprites; }
    }

    [SerializeField]
    private List<CellTypeSprite> bonusTypeSprites;
    public List<CellTypeSprite> BonusTypeSprites
    {
        get { return bonusTypeSprites; }
    }
    [SerializeField]
    private float countDownTime;
    public float CountDownTime { get => countDownTime;}


    private Dictionary<TileSupertype, Dictionary<TileSubType, Sprite>> tileTypesDict;
    public Dictionary<TileSupertype, Dictionary<TileSubType, Sprite>> TileTypesDict { get => tileTypesDict; }

}
