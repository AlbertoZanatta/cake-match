﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusButtonController : MonoBehaviour
{
    [SerializeField]
    private TileSubType tileSubType;
    [SerializeField]
    private Image image;
    [SerializeField]
    private Animator explosionAnimator;
    // Start is called before the first frame update
    private void Start()
    {
        image.sprite = GameController.instance.TileTypesDict[TileSupertype.BONUS][tileSubType];
    }

    // Update is called once per frame
    public void Clicked()
    {
        GameController.instance.SetBonusType(tileSubType);

        if (tileSubType == TileSubType.TYPE1)
        {
            explosionAnimator.SetTrigger("BombExplosion");
        }
        else if (tileSubType == TileSubType.TYPE2)
        {
            explosionAnimator.SetTrigger("IceExplosion");
        }
    }
}
