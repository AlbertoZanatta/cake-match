﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TimeLeftController : MonoBehaviour
{
    TextMeshProUGUI timeText;

    private void Awake()
    {
        timeText = this.GetComponent<TextMeshProUGUI>();
    }

    public void SetTimeText(float seconds)
    {
        int minutesLeft = (int)(seconds / 60f);
        string minutesLeftString = minutesLeft.ToString().Length == 1 ? "0" + minutesLeft : minutesLeft.ToString();
        int secondsLeft = (int)(seconds - (float)minutesLeft * 60f);
        string secondsLeftString = secondsLeft.ToString().Length == 1 ? "0" + secondsLeft : secondsLeft.ToString();
        timeText.text = minutesLeftString + "m:" + secondsLeftString + "s";
    }
}
