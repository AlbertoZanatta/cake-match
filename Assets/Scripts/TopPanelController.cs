﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopPanelController : MonoBehaviour
{
    [SerializeField]
    Animator topPanelAnimator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Freeze()
    {
        topPanelAnimator.SetBool("isFrozen", true);
    }

    public void Defreeze()
    {
        topPanelAnimator.SetBool("isFrozen", false);
    }
}
